# Eqiva

Documentazione del costruttore/venditore
----------------------------------------

* [Manuale (EN) PDF](https://www.eq-3.com/downloads/download/eqiva/bda/CC-RT-M-BLE-EQ_UM_E_eqiva_180508.pdf)


Produttore/venditore

    eQ-3 AG
    Maiburger Straße 29
    26789 Leer / GERMANY
    https://www.eQ-3.de


Caratteristiche/funzionalità di base
------------------------------------
* Device short description: CC-RT-M-BLE-EQ
* Supply voltage:     2x 1.5 V LR6/mignon/AA
* Current consumption:  100 mA max.
* Battery life: 2 years (typ.)
* Degree of protection:   IP20
* Degree of pollution:   2
* Ambient temperature:  5 to 35 °C
* Surface temperature:   90 °C (at the radiator)
* Display:    LCD
* Connection: M30 x 1.5 mm
* Method of operation:   Type 1
* Linear travel:      4.3 mm
* Dimensions (W x H x D): 58 x 63 x 122 mm
* Weight:    176 g (incl. batteries)
* Radio frequency:    2.402 GHz - 2.480 GHz
* Open area RF range:   10 m (typ.)

Links utili
-----------
* [Una guida per intercettare e decifrare la comunicazione tra la app e l'apparecchio](https://www.torsten-traenkner.de/wissen/smarthome/heizung.php)
* [Convertire da HEX a DEC e viceversa](https://www.rapidtables.com/convert/number/hex-to-decimal.html?x=17)



Prepararsi al collegamento Bluethooth
=====================================

Verificare che il Bluethooth del proprio PC non sia impegnato
-------------------------------------------------------------

    rfkill list bluetooth


Elencare i bluethooth disponibili sul PC
----------------------
NB: per Raspberry 1 e 2 è necessario un USB bluetooth stick

    hciconfig -a

Attivare il bluethooth
----------------------

    sudo  hciconfig hci0 up

Come ottener il MAC del bluethooth del proprio PC
---------------------------------------------------

    $ hcitool dev
    Devices:
    hci0        DC:A6:32:0F:7E:00

Cerca in aria quali apparecchi BLE sono raggiungibili
-----------------------------------------------------

    $ sudo hcitool -i hci0 lescan
    LE Scan ...
    00:1A:22:11:F5:AC (unknown)
    00:1A:22:11:F5:AC CC-RT-BLE
    ^C

Come sapere chi ha prodotto il pezzo bluethoth
---------------------------------------------------
La prima parte dell'indirizzo MAC "appartiene" ad un produttore.
Al link
[IEEE website](https://regauth.standards.ieee.org/standards-ra-web/pub/view.html#registries)
si può scoprire chi è.

Selezionare  "All MAC" e poi mettere come filtro la prima parte del MAC, senza spazi, 
p.es. "dca632" o "001a22".

* DC:A6:32 -> Raspberry Pi Trading Ltd.
* 00:1A:22 -> eQ-3 Entwicklung GmbH

Farsi dare informazioni di base sulla controparte
-------------------------------------------------

    $ sudo hcitool -i hci0 leinfo 00:1A:22:11:F5:AC
    Requesting information ...
	Handle: 64 (0x0040)
	LMP Version: 4.1 (0x7) LMP Subversion: 0x220f
	Manufacturer: Broadcom Corporation (15)
	Features: 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
    

Comunicare con il termostato usando gatttool
============================

Aprire un'interfaccia interattiva 
---------------------------------
    $ sudo gatttool -i hci0 -b 00:1A:22:11:F5:AC -I
    [00:1A:22:11:F5:AC][LE]> connect
    Attempting to connect to 00:1A:22:11:F5:AC
    Connection successful
    [00:1A:22:11:F5:AC][LE]> 


Listing device services
-----------------------

    [00:1A:22:11:F5:AC][LE]> primary
    attr handle: 0x0100, end grp handle: 0x0151 uuid: 00001800-0000-1000-8000-00805f9b34fb
    attr handle: 0x0200, end grp handle: 0x0220 uuid: 00001801-0000-1000-8000-00805f9b34fb
    attr handle: 0x0300, end grp handle: 0x0321 uuid: 0000180a-0000-1000-8000-00805f9b34fb
    attr handle: 0x0400, end grp handle: 0x0430 uuid: 3e135142-654f-9090-134a-a6ff5bb77046
    attr handle: 0xff00, end grp handle: 0xff07 uuid: 9e5d1e47-5c13-43a0-8635-82ad38a1386f
   


la prima parte dopo uuid:

    0x00001800: Generic Access
    0x00001801: Generic Attribute
    0x0000180a: Device Information
    0x3e135142: ?????
    0x9e5d1e47: ?????


   
List characteristics
-------------------    

Le characteristic properties (char properties) sono descritte da un Byte.
Se il secondo bit (quello del "2") è messo a 1, allora
la caratteristica può essere letta.
Se il quarto bit (quello dell' "8") è messo a 1, allora
la caratteristicha può essere scritta.

Se ci sono entrambi i bit (2+8=10 = 0x0a) allora sono read+write.

    0x02  ------1-  read only
    0x08  ----1---  write only
    0x0a  ----1-1-  read and write
    0x20  --1-----  ???

La tabella complessiva di tale Byte ha i seguenti significati

    0x00  --------  Type unknow
    0x01  -------1  Broadcast of Characteristic value permitted
    0x02  ------1-  Read permitted
    0x04  -----1--  Write without response permitted
    0x08  ----1---  Write permitted
    0x10  ---1----  Notification permitted
    0x20  --1-----  Indications permitted
    0x40  -1------  Signed write permitted
    0x80  1-------  Extended proprieties, definited in the 
                    Charaacteristics Extended Proprieties Descriptor


Generic Attribute (0x1801) 
--------------------------
    [00:1A:22:11:F5:AC][LE]> characteristics 0200 0220
    handle: 0x0210, char properties: 0x22, char value handle: 0x0211, uuid: 00002a05-0000-1000-8000-00805f9b34fb

Essendo 0x22 ci sono diritti di lettura e inoltre "indications permitted"

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0211
    Characteristic value/descriptor: 00 00 00 00 

Generic Access (0x1800)
-----------------------

    [00:1A:22:11:F5:AC][LE]> characteristics 0100 0151
    handle: 0x0110, char properties: 0x02, char value handle: 0x0111, uuid: 00002a00-0000-1000-8000-00805f9b34fb
    handle: 0x0120, char properties: 0x02, char value handle: 0x0121, uuid: 00002a01-0000-1000-8000-00805f9b34fb
    handle: 0x0130, char properties: 0x02, char value handle: 0x0131, uuid: 00002a02-0000-1000-8000-00805f9b34fb
    handle: 0x0140, char properties: 0x08, char value handle: 0x0141, uuid: 00002a03-0000-1000-8000-00805f9b34fb
    handle: 0x0150, char properties: 0x02, char value handle: 0x0151, uuid: 00002a04-0000-1000-8000-00805f9b34fb

### Device Name (hnd 0x0111; uuid 0x2a00)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0111
    Characteristic value/descriptor: 43 43 2d 52 54 2d 42 4c 45

tradotto da utf8 significa (vedasi: https://www.utf8-chartable.de/)

    CC-RT-BLE


### Appearance (hnd 0x0121; uuid 0x2a01)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0121
    Characteristic value/descriptor: 00 00 

### ??? (hnd 0x0131; uuid 0x2a02)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0131
    Characteristic value/descriptor: 00 


### ???  (hnd 0x0141; uuid 0x2a03)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0141
    Error: Characteristic value/descriptor read failed: Attribute can't be read


### ???  (hnd 0x0151; uuid 0x2a04)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0151
    Characteristic value/descriptor: 00 00 00 00 00 00 00 00


Device Information (0X180A)
---------------------------
    [00:1A:22:11:F5:AC][LE]> characteristics 0300 0321
    handle: 0x0310, char properties: 0x02, char value handle: 0x0311, uuid: 00002a29-0000-1000-8000-00805f9b34fb
    handle: 0x0320, char properties: 0x02, char value handle: 0x0321, uuid: 00002a24-0000-1000-8000-00805f9b34fb

### Manufacturer Name (uuid 0x002a29)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0311
    Characteristic value/descriptor: 65 71 2d 33

tradotto, vuol dire 

    eq-3    

### Model Number (uuid 0x00002a24)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0321
    Characteristic value/descriptor: 43 43 2d 52 54 2d 42 4c 45  

tradoto, vuol dire 

    CC-RT-BLE


???? uuid 3e135142-654f-9090-134a-a6ff5bb77046
-------------------------------------------------------------
    attr handle: 0x0400, end grp handle: 0x0430 uuid: 3e135142-654f-9090-134a-a6ff5bb77046

    [00:1A:22:11:F5:AC][LE]> characteristics 0400 0430
    handle: 0x0410, char properties: 0x0a, char value handle: 0x0411, uuid: 3fa4585a-ce4a-3bad-db4b-b8df8179ea09
    handle: 0x0420, char properties: 0x1a, char value handle: 0x0421, uuid: d0e8434d-cd29-0996-af41-6c90f4e0eb2a

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0411
    Characteristic value/descriptor: 03 14 01 1a 0f 19 33 00 00 00 00 00 00 00 00 00 
    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0421
    Characteristic value/descriptor: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 

### Risposta all'ultimo handle chiamato

                                                 ++##
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 1307
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 02 01 28 00 04 22 00 00 00 00 2e 02 34 23 07 

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0411
    Characteristic value/descriptor: 13 07 01 1a 15 30 00 00 00 00 00 00 00 00 00 00 
                                     ++ ##  1 26 21 48 
                                     hn val m dd hh min

### orologio interno? (hnd 0411,  uuid 3fa4585a-ce4a-3bad-db4b-b8df8179ea09)

    [00:1A:22:11:F5:AC][LE]> char-read-hnd 0411
    Characteristic value/descriptor: 45 00 01 1a 12 04 06 00 00 00 00 00 00 00 00 00 

    Byte  n   esempio   significato
    0     1   03 ->  3  ? ultimo handle chiamato
    1     2   14 -> 20  ? anno (dopo 2000)
    2     3   01 ->  1  ? mese, (1=gennaio)
    3     4   1a -> 26  ? giorno (del mese)
    4     5   12 -> 18  ? ora? di cosa? dell'ultima modifica?
    5     6   1a -> 26  ? minuti? di cosa? dell'ultima modifica?
    6     7   0d -> 13  ? cosa?

### status (hnd 0421, uuid d0e8434d-cd29-0996-af41-6c90f4e0eb2a)

     Byte  n   esempio   significato
     0     1   02 ->  2  ? 0000 0010
     1     2   01 ->  1  ? 0000 0001
     2     3   03 ->  3  ? modalità attiva (automatic, manual, boost, assenza, lock)
     3     4   03 ->  3  ? apertura della valvola con valori tra 0 e 99% (0x00 e 0x63)
     4     5   03 ->  3  ?
     5     6   26 -> 38  ? temperatura di riferimento in "azione"
     6     7   1a -> 26  ? fine modalità "assenza": giorno 
     7     8   14 -> 20  ? fine modalità "assenza": anno (dopo 2000; 20 -> 2020)
     8     9   1e -> 31  ? fine modalità "assenza": ora (in 1/2 ore; 31 = 2 x 15.5 = 15h30', 15:30)
     9    10   01 ->  1  ? fine modalità "assenza": mese (1=gennaio)
    10    11   03 ->  3  ?
    11    12   03 ->  3  ? "window": temperatura (in 1/2 gradi)
    12    13   03 ->  3  ? "window": durata (in 1/2 ore)
    13    14   03 ->  3  ? temperatura default modalità "giorno"="comfort", espressa in 1/2 gradi
    14    15   03 ->  3  ? temperatura default modalità "eco"="notte"="risparmio", espressa in 1/2 gradi
    15    16   07 ->  7  ? 0000 1111

#### terzo Byte - modalità attivate (automatic, manual, boost, assenza, lock)

    ---- ---0 00   0     0=automatic
    ---- ---1 01   1     1=manual
    ---- --X- 02   2  urlaubsmodus (0=presente 1=assente)
    ---- -X-- 04   4  boost 0=off 1=on
    ---- X--- 08   8  ? orario estivo/invernale
    ---1 ---- 10  16  modalita finestra aperta
    --1- ---- 20  32  blocco tastierino (0=unlock, 1=lock)
    -X-- ---- 40  64  ?
    X--- ---- 80 128  ? batterie scariche
    
    

#### quarto Byte - apertura valvola

pare che vada da 0 a 100, ovvero da 0x00 a 0x64

#### sesto Byte
Si può conoscere il valori della temperatura di riferimento, espressa in 1/2 gradi
(0x26=38=19.0°C, 0x32=50=25.0°C)



### Programma per ciascun giorno della settimana

* Comincia sempre con 21
* il secondo Byte indica il giorno della settimana (0=sabato, 2=lunedi)
* seguono coppie di due Byte, formate da temperatura e orario fine validità
    * dunque il terzo Byte indica la temperatura a partire dalle ore 00:00
    * e l'ultimo Byte della serie è sempre 90 (=24:00)

* dalla App è possibile impostare per ciascun giorno della settiman
    * una temperatura "bassa"
    * fino a 6 intervalli (precisione: 10') con una propria temperatura (precisione 1/2 grado)

     Byte  n   esempio   significato
     0     1   21 -> 33  ? 0010 0001
     1     2   02 ->  2  ? giorno della settimana (0=sabato, 2=lunedi)
     2     3   03 ->  3  ? temperatura valida (in 1/2 gradi) a partire dalle ore 00:00
     3     4   03 ->  3  ? ora (in 1/6 di ore) cambio temperatura
     4     5   03 ->  3  ? temperatura valida (in 1/2 gradi)
     3     4   03 ->  3  ? ora (in 1/6 di ore) cambio temperatura
     4     5   03 ->  3  ? temperatura valida (in 1/2 gradi)
     5     6   90 ->144  ? 24:00 (1440 minuti = 24 ore) 
     6     7   00 ->  0  ? 
     7     8   00 ->  0  ? 
     8     9   00 ->  0  ? 
     9    10   00 ->  0  ? 
    10    11   00 ->  0  ? 
    11    12   00 ->  0  ? 
    12    13   00 ->  0  ? 
    13    14   00 ->  0  ? 
    14    15   00 ->  0  ? 
    15    16   00 ->  0  ? 
    

???? uuid 9e5d1e47-5c13-43a0-8635-82ad38a1386f
-----------------------------------------------------------

    attr handle: 0xff00, end grp handle: 0xff07 uuid: 9e5d1e47-5c13-43a0-8635-82ad38a1386f    

    [00:1A:22:11:F5:AC][LE]> characteristics ff00 ff07
    handle: 0xff01, char properties: 0x38, char value handle: 0xff02, uuid: e3dd50bf-f7a7-4e99-838e-570a086c666b
    handle: 0xff04, char properties: 0x08, char value handle: 0xff05, uuid: 92e86c7a-d961-4091-b74f-2409e72efe36
    handle: 0xff06, char properties: 0x02, char value handle: 0xff07, uuid: 347f7608-2e2d-47eb-913b-75d4edc4de3b
   
    [00:1A:22:11:F5:AC][LE]> char-read-hnd ff02
    Error: Characteristic value/descriptor read failed: Attribute can't be read
    [00:1A:22:11:F5:AC][LE]> char-read-hnd ff05
    Error: Characteristic value/descriptor read failed: Attribute can't be read
    [00:1A:22:11:F5:AC][LE]> char-read-hnd ff07
    Characteristic value/descriptor: 00 10 03 02 


Interagire tramite gatttool, per leggere e impostare valori
===========================================================

Riassunto
---------

Per leggere e scrivere valori, bisogna scrivere al handle 0x0411

Sono previsti i seguenti codici

    0411 00 - 0000 0000 - R  - numero di serie
    0411 03 - 0000 0011 -  W - ora esatta   
    0411 10 - 0001 0000 -  W - modificare il settaggio dei singoli giorni settimanali
    0411 11 - 0001 0001 -  W - Modificare le temperature "bassa" e "alta" di default
    0411 13 - 0001 0011 -  W - offset
    0411 14 - 0001 0100 -  W - "finestra aperta"
    0411 20 - 0010 0000 - R - vedere il settaggio dei singoli giorni settimanali
    0411 40 - 0100 0000 -  W - impostare automatic/manual/holiday
    0411 41 - 0100 0001 -  W - Scegli come temperatura di riferimento una ad hoc (da indicare)
    0411 43 - 0100 0011 -  W - Scegli come temperatura di riferimento quella "alta"  
    0411 44 - 0100 0100 -  W - Scegli come temperatura di riferimento quella "bassa" 
    0411 45 - 0100 0101 -  W - attiva/disattiva Boost
    0411 80 - 1000 0000 -  W - blocca7sblocca tastierino 

Per impostare l'ora esatta
-----------------------

    03    codice da usare
    0314011a153000   2020-01-26 21:48:00  
      | | | | | |
      | | | | | +--- secondi (0x00 = 00)
      | | | | +----- minuti  (0x30 = 48)
      | | | +------- ora     (0x15 = 21)
      | | +--------- giorno  (0x1a = 26)
      | +----------- mese    (0x01 = 1 = gennaio)
      +------------- anno    (0x14 = 20 => 2020)

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 0314011a153000
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 02 01 28 00 04 22 00 00 00 00 2e 02 34 23 07 


char-write-req 0411 11 - Modificare le temperature "bassa" e "alta" di default
-----------------------------------------------------
Sono interessati il penultimo e il terzultimo Byte

    11     codice per impostare
    11ab   imposta la temperatura "alta" a 0xAB
    11abcd imposta la temperatura "alta" a 0xAB e quella "bassa" a 0xCD (in 1/2 gradi)


per impostare

* "alta" = 26.0°C => 2 * 26.0 = 52 = 0x34 (terzultimo Byte)
* "bassa" = 17.5°C => 2 * 17.5 = 35 = =x23 (penultimo Byte)

    .                                              ____
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 113423
    Characteristic value was written successfully                           __ __
    Notification handle = 0x0421 value: 02 01 08 00 04 01 00 00 00 00 14 01 34 23 09 


char-write-req 0411 4000 - imposta modalità "automatic"
-------------------------------------------------------

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 4000
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 02 01 28 00 04 20 00 00 00 00 2e 02 34 23 07
                                               |   _
                                           0010 1000


char-write-req 0411 4040 - imposta modalità "manuale"
-------------------------------------------------------

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 4040
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 02 01 29 00 04 20 00 00 00 00 2e 02 34 23 07
                                               |   _
                                           0010 1001


char-write-req 0411 40abcdef1289 - imposta modalità "vacanze"
-------------------------------------------------------
Bisogna indicare
* la temperatura desiderata     (16.0°C = 0.5 * 32 => 32=0x20)
* fino a quando: giorno         (26 = 0x1a)
* fino a quando: anno           (2020 = 2000 + 20 => 20=0x14)
* fino a quando: ora (1/2 ore)  (23:30 = 23.5 = 1/2 * 47 => 47=0x2f)
* fino a quando: mese           (1 = 0x01)


    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 40201a142f01
    Characteristic value was written successfully      __
    Notification handle = 0x0421 value: 02 01 28 62 04 20 00 00 00 00 2e 02 34 23 07 
                                                       
                                                 

char-write-req 0411 43|44 - seleziona la temperatura di riferimento
-----------------------------------------------------------------
Il Byte interessato è il sesto. 
Vengono prelevati i valori dal penultimo e terzultimo Byte
oppure ne viene indicato una esplicitamente.
Pare che le temperature 4.5°C (0x09) e 30.0°C (0x3c) siano da considerare come
permanentemente OFF e ON (oltre ad essere i valori limite di validità)

    43     codice per impostare come temperatura di riferimento quella "alta"  (terzultimo Byte)
    44     codice per impostare come temperatura di riferimento quella "bassa" (penultimo Byte)
    41     codice per selezionare una ad hoc
    41ab   imposta la temperatura di riferimento a 0xAB (in 1/2 gradi) (0x30=48 => 24.0°C) (0x30=48 => 24.0°C)

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 43    
    Characteristic value was written successfully      __                   ||   
    Notification handle = 0x0421 value: 02 01 28 00 04 34 00 00 00 00 14 01 34 23 09 

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 44    
    Characteristic value was written successfully      __                      ||
    Notification handle = 0x0421 value: 02 01 28 00 04 23 00 00 00 00 14 01 34 23 09

                                                   __
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 4130  
    Characteristic value was written successfully      __
    Notification handle = 0x0421 value: 02 01 28 00 04 30 00 00 00 00 14 01 34 23 09

char-write-req 0411 08ab - LOCK/UNLOCK
--------------------------------------
È interessato il terzo Byte

    80     codice per impostare
    8000   UNLOCK
    8001   LOCK

    .                                              __
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 8001
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 02 01 28 00 04 01 00 00 00 00 14 01 34 23 09
    .                                              __
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 8000
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 02 01 08 00 04 01 00 00 00 00 14 01 34 23 09

char-write-req 0411 45ab - BOOST ON/OFF
---------------------------------------
Pare che nel terzo Byte venga attivato/disattivato 
il bit del 4, ovvero il sesto bit ovvero, visto da destra, il terzo bit (---- -X--).
È interessato anche il quarto Byte, in quanto viene aperta/chiusa la valvola.

    45     codice da impostare
    4501   set boost=ON
    4500   set boost=OFF

                                                   __
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 4501
    Characteristic value was written successfully__
    Notification handle = 0x0421 value: 02 01 2c 50 04 01 00 00 00 00 14 01 34 23 09 
                                               + ==
                                           0010 1100   
                                                   __
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 4500
    Characteristic value was written successfully__
    Notification handle = 0x0421 value: 02 01 28 00 04 01 00 00 00 00 14 01 34 23 09 
                                               + ==
                                           0010 1000    

char-write-req 0411 20ab - Vedere impostazioni per ciascun singolo giorno della settimana
---------------------------------------------------------------
I giorni della settimana sono numerati in modo prograssivo, cominciando con 0=sabato, ..., 2=lunedi, ..., 6=venerdi
Nella risposta, il secondo Byte indica il giorno della settimana.
Seguono poi coppie di Byte che indicano periodi con temperature differenti (temperatura;fine)
La prima temperatura vale a partire dalle ore 00:00. L'ultimo Byte utile ha il valore 0x90 in quanto
0x90=144 => 1440 minuti => 24 ore => 24:00

In totale ci sono dunque al massimo 7 differenti intervalli

    20    codice da impostare
    20ab  vedere impostazioni del giorno ab (ab = 00, .., 06)

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2000
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 00 22 90 00 00 00 00 00 00 00 00 00 00 00 00
                                           ww ++ ||

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2001
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 01 34 60 20 90 00 00 00 00 00 00 00 00 00 00 
                                           ww ++ -- ++ || 

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2002
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 02 1b 4e 34 90 00 00 00 00 00 00 00 00 00 00 
                                           ww ++ -- ++ || 

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2003
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 03 20 24 2e 29 20 4b 2e 7e 20 90 00 00 00 00 
                                           ww ++ -- ++ -- ++ -- ++ -- ++ || 

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2004
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 04 1d 24 2e 2a 1d 60 30 90 00 00 00 00 00 00 
                                           ww ++ -- ++ -- ++ -- ++ || 

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2005
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 05 24 24 30 29 24 90 00 00 00 00 00 00 00 00 
                                           ww ++ -- ++ -- ++ || 

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 2006
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 21 06 22 48 29 60 22 90 00 00 00 00 00 00 00 00
                                           ww ++ -- ++ -- ++ || 


char-write-req 0411 10ab.... - Modificare impostazioni per ciascun singolo giorno della settimana
---------------------------------------------------------------
Corrisponde alla versione "lettura" 

* si comincia con 10 (non con 20, valido per la "lettura")
* il secondo Byte indica il giorno della settimana
* è necessario indicare il contenuto di tutti i 14 Byte che seguono, a coppie di (temp,orario)

Come in "lettura", vale anche qui:

* 0=sabato, ..., 1=lunedi, ..., 6=venerdi
* temperatura misurata in 1/2 gradi
* orario indicato in 10' (ovvero 1/6 di ora)



char-write-req 0411 14abcd - "Finestra aperta": impostare durata e temperatura
------------------------------------------------
Vengono modificati il quartultimo (durata, in 1/2 ora) e quintultimo (temperatura, in 1/2 gradi) Byte.

    14     codice per impostare
    14abcd imposta temperatura (0xAB) e durata (0xCD)

Come impostare a 23°C e 1 ora: 23.0=46*1/2 => 46=0x2e, 1=2*1/2 => 2=0x02

                                                   ____
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 142e02
    Characteristic value was written successfully                     __ __    
    Notification handle = 0x0421 value: 02 01 28 00 04 22 00 00 00 00 2e 02 34 23 09


char-write-req 13ab - OFFSET temperatura
----------------------------------------
È memorizzato nell'ultimo Byte. La temperatura viene memorizzata con la precisione di 1/2 grado.
I valori possibili vanno da 0x00 a 0x0f, ovvero da 0 a 15, dove 0x01=1 corrisponde a -3.5°C
e 0x0f=15 corrisponde a +3.5°C e 0x08=8 corrisponde a 0.0°C. Il confronto con i valori della app mostra 
una "insicurezza" di 1/4 di grado. A volte pare che lo zero sia 0x07.

    13     codice per impostare
    13ab   imposta offset (0xAB) con valori da 0x00 a 0x0f

                                                   __    
    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 1308
    Characteristic value was written successfully                                 __
    Notification handle = 0x0421 value: 02 01 28 00 04 22 00 00 00 00 2e 02 34 23 08    

char-write-req 0411 00 - Leggi il numero di serie
--------------------------------------------------
Dovrebbe coincidere con il codice che si trova nel vano batterie.

    [00:1A:22:11:F5:AC][LE]> char-write-req 0411 00
    Characteristic value was written successfully
    Notification handle = 0x0421 value: 01 78 00 00 80 75 81 61 67 60 60 61 69 65 99 
                                                    /  /  /   |  |  |  |  |  |  |
                                        ?  ?  ?  ?  P  K  Q   1  7  0  0  1  9  5  ?
